import csv
contacts=[]

def  view_all_contacts():
  with open('tellbook.csv','r') as file:
    file=csv.DictReader(file)
    for contact in file:
     print(f'Firstname: {contact["Firstname"]}')
     print(f'Lastname: {contact["Lastname"]}')
     print(f'Mobile: {contact["Mobile"]}')
     print(f'NationalNumber: {contact["NationalNumber"]}')
     print(f'Address: {contact["Address"]}')
     print('-'*30)
    return True

def find_contact():
    phone=input("Enter Phone: ")
    if phone and phone.isdigit():
     with open('tellbook.csv','r') as file:
      file=csv.DictReader(file)
      for contact in file:
       if contact['Mobile']==phone:
        print()
        print('-'*35)
        print(f"Firstname: {contact['Firstname']}")
        print(f"Lastname: {contact['Lastname']}")
        print(f"Mobile: {contact['Mobile']}")
        print(f"NationalNumber: {contact['NationalNumber']}")
        print(f"Address: {contact['Address']}")
        print('-'*35)
        
       else:
         print('Contact Not Found ! , Try Again !')
    else:
      raise ValueError('Empty Or None Numeric !')
try:
  while True:
    print('This Is Your Tell Book.')
    print()
    print('1- View All Contacts')
    print('2- View a Contact')
    print('3- Update Contact')
    print('4- Delete Contact')
    print('5- Add Contact')
    print('6- Exit')
    choice=int(input("Choose Your Action(1/2/3/4/5): ").strip())
    if  choice==1:
      view_all_contacts()
    elif  choice==6:
      exit()
    elif  choice==2:
      find_contact()
    


except Exception as e:
  print(e)
    

